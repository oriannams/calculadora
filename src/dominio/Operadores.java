package dominio;

public class Operadores{
	
	public static int factorial(int base){
		int total = 1;
		if(base > 0){
			for(int i = 1; i <= base; i++){
			total *= i;
			}
			return total;
		}else if(base < 0){
		//Falta hacerlo para los negativos
			return 0;
		}else{
			return 1;
		}
		}

	public static int potencias(int base, int exponente){
                int total = 1;
                for(int i = 1; i <= exponente; i++){
                	total *= base;
                }
                        return total;
                }
}

